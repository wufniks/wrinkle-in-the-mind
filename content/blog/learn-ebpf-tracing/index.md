---
title: 튜토리얼과 예제를 통한 eBPF를 이용한 트레이싱 배우기
date: "2019-04-02T19:03:32.169Z"
description: Poor translation of http://brendangregg.com/blog/2019-01-01/learn-ebpf-tracing.html
category: 'BPF'
draft: false
---

# 들어가는 말

Brendan Gregg의 http://brendangregg.com/blog/2019-01-01/learn-ebpf-tracing.html 을 읽은 내용을 번역해봅니다.

# 초급

## eBPF, bcc, bftrace, ioviosor는 무엇인가?

*eBPF*와 Linux의 관계는 JavaScript와 HTML에 비유할 수 있습니다. 정적인 HTML와 달리 JavaScript를 이용해 마우스 클릭 같은 이벤트 들에 대해 동작하고 브라우저의 안전한 가상머신 안에서 돌아가는 작은 프로그램을 정의하는것이 가능합니다. 한편 eBPF를 이용하면 정해진 커널과 달리 디스크 입출력 같은 이벤트들에 대해 동작하고 커널의 안전한 가상머신 안에서 돌아가는 작은 프로그램을 작성하는것이 가능합니다. 실제로는 eBPF는 JavaScript보다는 그것을 실행하는 v8 가상 머신에 더 가깝습니다. eBPF는 리눅스 커널의 한 부분입니다.

eBPF로 프로그래밍하는것은 v8 바이트코드로 코딩하는 것만큼이나 무척 어렵습니다. 하지만 누구도 v8로 직접 코딩하지 않죠. 대체로 JavaScript나 JavaScript로 이루어진 프레임웍들(jQuery, Angular, React등)을 사용합니다. eBPF의 경우에도 마찬가지 입니다. 프레임웍을 통해 eBPF를 사용하고 코딩합니다. 트레이싱에 경우 bcc와 bpftrace가 주요한 프레임웍입니다. 이것들은 커널 코드에 포함되어 있지는 않고 iovisor고 불리는 github 프로젝트로 Linux Foundation에서 관리됩니다.

## eBPF 트레이싱의 예제

이 eBPF 기반 툴은 연결된 TCP 세션들을 보여줍니다. 연관된 프로세스의 ID(PID), 커맨드 이름(COMM), 주고 받은 크기(TX_KB, RX_KB), 지속시간(MS, 밀리초 단위)의 정보를 포함합니다.

```
# tcplife
PID   COMM       LADDR           LPORT RADDR           RPORT TX_KB RX_KB MS
22597 recordProg 127.0.0.1       46644 127.0.0.1       28527     0     0 0.23
3277  redis-serv 127.0.0.1       28527 127.0.0.1       46644     0     0 0.28
22598 curl       100.66.3.172    61620 52.205.89.26    80        0     1 91.79
22604 curl       100.66.3.172    44400 52.204.43.121   80        0     1 121.38
22624 recordProg 127.0.0.1       46648 127.0.0.1       28527     0     0 0.22
3277  redis-serv 127.0.0.1       28527 127.0.0.1       46648     0     0 0.27
22647 recordProg 127.0.0.1       46650 127.0.0.1       28527     0     0 0.21
3277  redis-serv 127.0.0.1       28527 127.0.0.1       46650     0     0 0.26
[...]
```

이런 기능이 eBPF를 이용해야만 가능한 일은 아닙니다. 기존의 커널 관련 기술들을 이용해 tcplife를 작성하는것도 가능합니다. 하지만 그렇게 했다면 성능 오버헤드나 보안 이슈로 인해 프로덕션에서 사용하지 못했을 것입니다. eBPF가 가능하게 하는건 이 툴을 실용적으로(효율적이고 안전하게) 만드는 것입니다. 예를 들어, 기존에 방식처럼 모든 패킷을 추적해서 성능상의 큰 오버헤드가 발생시키지 않습니다. 대신 TCP session만을 추척하는데, 이는 훨씬 덜 빈번합니다. 이러한 방식으로 오버헤드가 아주 작아지게 되어 프로덕션에서 항상 실행하는것이 가능하게 됩니다.

## 어떻게 사용하나?

초급자들은 bcc툴을 먼저 사용해보십시오. 여러분의 OS에 대한 이곳의 [설치 메뉴얼|https://github.com/iovisor/bcc/blob/master/INSTALL.md]을 참조하십시오. Ubuntu에서는 이런 식입니다.

```
# sudo apt-get update
# sudo apt-get install bpfcc-tools
# sudo /usr/share/bcc/tools/opensnoop
PID    COMM               FD ERR PATH
25548  gnome-shell        33   0 /proc/self/stat
10190  opensnoop          -1   2 /usr/lib/python2.7/encodings/ascii.x86_64-linux-gnu.so
10190  opensnoop          -1   2 /usr/lib/python2.7/encodings/ascii.so
10190  opensnoop          -1   2 /usr/lib/python2.7/encodings/asciimodule.so
10190  opensnoop          18   0 /usr/lib/python2.7/encodings/ascii.py
10190  opensnoop          19   0 /usr/lib/python2.7/encodings/ascii.pyc
25548  gnome-shell        33   0 /proc/self/stat
29588  device poll         4   0 /dev/bus/usb
^C
```

위에서 opensnoop을 실행시켜서 툴들이 잘 동작하는지 확인했습니다. 여기까지 왔다면 여러분은 eBPF를 사용한 것입니다.

넷플릭스나 페이스북을 포함한 회사들은 bcc를 모든 서버에 기본으로 설치합니다. 여러분도 앞으로 그럴지도 몰라요.

## 초급자를 위한 튜토리얼이 있나요?

네. Brendan Gregg이 만든 eBPF 트레이싱의 초급자에게 좋은 시작점이 될만한 튜토리얼이 여기 있습니다.

- [bcc tutorial|https://github.com/iovisor/bcc/blob/master/docs/tutorial.md]

초급자라면 eBPF 코드를 직접 작성할 필요는 없습니다. bcc는 기본적으로 70개가 넘는 툴들을 포함하고 있습니다. 튜토리얼에서는 그중에 execsnoop, opensnoop, ext4slower(btrfs*, xfs*, zfs*), biolatency를 포함해서 열한개를 사용해보게 됩니다.

일단 튜토리얼을 마치고나면, 그 외에도 훨씬 많은 툴들이 있다는 사실만 알고 있으면 됩니다.

![bcc tracing tools](./bcc_tracing_tools.png)

모든 툴들 또한 메뉴얼 페이지와 예제 파일로 완벽하게 문서화 되어 있습니다. 예제 파일들(bcc_tools안의 *_example.txt 파일들)은 설명과 함께 스크린샷을 보여줍니다.(예. [biolatency_exmaple.txt|https://github.com/iovisor/bcc/blob/master/tools/biolatency_example.txt]) 다수의 예제 파일들(과 메뉴얼 페이지와 툴)을 Brendan Gregg이 작성했습니다. 각각 하나의 블로그 글이라고도 할 수 있는 예제 파일들을 bcc 저장소에서 찾아 볼 수 있습니다.

현재 미진한 부분은 프로덕션에서의 예제입니다. 모든 문서들이 eBPF가 갓 만들어졌을때 작성되었기 때문에 테스트 인스턴스에서만 사용가능했습니다. 따라서 대부분이 만들어진 예제입니다. 시간이 지남에 따라 실제 예제들을 추가할 것입니다. 이 부분이 초급자들도 도울수 있는 영역입니다. 만약에 당신이 툴들로 이슈를 해결한다면 게시물을 작성해서 스크린샷을 공유하거나 그것들을 예제 파일에 추가하는것을 고려해주기 바랍니다.

# 중급

작성중

# 고급

작성중
